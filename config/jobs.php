<?php

use App\Jobs\LaunchCollecte;
use App\Lib\InteractiveJobs\JobDefinition;

return [
    'context' => null,
    'definitions' => [
        JobDefinition::create(LaunchCollecte::class, 'jobs.forms.crawl')
            ->setTitle('Url to Crawl')
            ->setRules([
                'nom' => 'string',
            ])
            ->onQueue("long-running-queue")
    ]
];
