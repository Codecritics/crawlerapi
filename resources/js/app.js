require('./bootstrap');
import Vue from 'vue';

Vue.config.devtools = true;

import ElementUI from 'element-ui';
import VueEcho from 'vue-echo';
import axios from 'axios';
import Toasted from 'vue-toasted';
import {Select, Option} from 'element-ui'
import ActiveJob from './components/ActiveJob';
import WatchActive from './components/WatchActive';
import Parameters from './components/Parameters'

import lang from 'element-ui/lib/locale/lang/fr'
import locale from 'element-ui/lib/locale'

locale.use(lang);

Vue.use(Toasted, {iconPack: 'fontawesome', theme: 'bubble', icon: 'fa-info-circle', className: 'app-toasted'});

Vue.use(VueEcho, {
    broadcaster: 'socket.io',
    host: window.location.hostname + ':6001'
});

Vue.component('active-job', ActiveJob);
Vue.use(ElementUI)
Vue.component('parameters', Parameters);

Object.defineProperty(
    Vue.prototype,
    '$user', {value: window.appConfig.auth},
    '$http', {
        value: axios.create({
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'X-CSRF-TOKEN': document.head.querySelector('meta[name="csrf-token"]').content
            }
        })
    });

const app = new Vue({
    el: '#app',
    components: {ActiveJob, WatchActive},
    data() {
        return {
            isAuthorized: false
        }
    },
    mounted() {
        if (this.$user !== null) {
            this.isAuthorized = true;
            this.subscribeUser();
        }
    },
    methods: {
        subscribeUser() {
            this.$echo.private(`User.${this.$user.id}`).notification((notification) => {
                this.$toasted.show(notification.message, {
                    type: notification.message_type
                }).goAway(5000);
            });
        }
    }
});
