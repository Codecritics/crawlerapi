<div class="form-group form-check">
    <label for="url">Url address</label>
    <input type="url" name="url" value="https://decisionnel.com/" class="form-control" id="url-field"
           aria-describedby="urlHelp"
           placeholder="https://">
    <small id="urlHelp" class="form-text text-muted">Enter the real url</small>
    <hr>
    <div class="row">
        <div class="col">
            <label for="depth_field">Depth of the Crawler</label>
            <input type="number" min="1" max="3" value="3" class="form-control" name="depth" id="depth_field">
        </div>
        <div class="col">
            <label for="docLimit_field">Number limitation of the Crawler</label>
            <input type="number" value="1000" class="form-control" name="docLimit" id="docLimit_field">
        </div>
        <div class="col">
            <label for="language_field">Language</label>
            <input type="text" value="FR" class="form-control" name="language" id="language_field">
        </div>

        <div class="col">
            <label for="excludedUrls">Excluded Urls</label>
            <input type="text" value="url1,url2,url3" class="form-control" name="excludedUrls" id="excludedUrls">
        </div>
    </div>
    <hr>
    <br>
    <h5>Indexation Parameters Base on Origin URL</h5>
    <!--
    <br>
    <div class="row">
        <div class="col">
            <label for="fonctions_field">Fonctions</label>
            <input type="text" value="F-1" class="form-control" name="fonctions" id="fonctions_field">
        </div>
        <div class="col">
            <label for="secteurs_field">Secteurs</label>
            <input type="text" value="S-1" class="form-control" name="secteurs" id="secteurs_field">
        </div>
        <div class="col">
            <label for="themes_field">Thèmes</label>
            <input type="text" value="T-1" class="form-control" name="themes" id="themes_field">
        </div>
        <div class="col">
            <label for="typesInfos_field">Types D'infos</label>
            <input type="text" value="TI-1" class="form-control" name="typesInfos" id="typesInfos_field">
        </div>
    </div>
    !-->
    <br>
    <div class="row">
        <div class="col">
            <label for="fonctions_select">Fonctions</label>
            <parameters name="fonctions" id="fonctions_select" class="col" placeholder="Fonctions"></parameters>
        </div>
        <div class="col">
            <label for="secteurs_select">Secteurs</label>
            <parameters name="secteurs" id="secteurs_select" placeholder="Sections"></parameters>
        </div>
        <div class="col">
            <label for="themes_select">Thèmes</label>
            <parameters name="themes" id="themes_select" placeholder="Thèmes"></parameters>
        </div>
        <div class="col">
            <label for="typesInfos_select">Types D'infos</label>
            <parameters name="typesInfos" id="typesInfos_select" placeholder="Types Infos"></parameters>
        </div>

    </div>
</div>
