![Image](./public/img/dream-logo.png)

# WEB-LBM

The RESTful API of the Crawler of DReAM

## Getting Started

To Launch this project, there are some few requirements needed. You'll need PHP 7.2, MySQL 5.6, Redis, Node.js v8.16.
But you will also need some package/dependencies manager: ***Composer***  for PHP dependencies and **Yarn/NPM** for 
Javascript dependencies.


### Prerequisites

As the main backEnd language we use **PHP** but some microservices are predicted as well like some
Nodejs lambda functions

### Installing

Installing the PHP Dependencies

```
composer install
```

Setting up the environment project:

```
rm -rf env. && mv .env.prod  .env
```

Setting up the database project:

```
php artisan migrate
```

or if you want to populate the database use seed parameter like below:

```
php artisan migrate --seed
```

Running Horizon to monitor the jobs:

```
php artisan horizon
```

Make sure to launch workers in order to execute jobs. (You'll need Redis installed for that)

```
php artisan queue:work --queue=default,notify
```

Installing Javascript dependencies (if you use yarn command try the following command) or if  you use npm use the 
command below

```
yarn && yarn run prod
```

```
npm install && npm run prod
```

Running Laravel Echo-Server to manage the notifications:

```
./node_modules/.bin/laravel-echo-server start
```




End with an example of getting some data out of the system or using it for a little demo

## Usage

This will be done if the next days.

## Running the tests

This will be done if the next days.

### Break down into end to end tests

This will be done if the next days.

```
Give an example
```

### And coding style tests

This will be done if the next days.

```
Give an example
```

## Deployment

This will be done if the next days.

## Built With

* [PHP](http://www.dropwizard.io/1.0.2/docs/) - A Powerful Language.
* [Composer](https://getcomposer.org/doc/) - Dependency Management

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Marius Tanawa Tsamo** - *Web Developer* - [Codecritics](https://github.com/codecritics)

See also the list of [contributors](https://github.com) who participated in this project.

## License

This is a personal repository.

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc
