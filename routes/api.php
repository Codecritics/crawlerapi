<?php

use App\Http\Controllers\CollecteRelationshipController;
use App\Http\Controllers\JobController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('collectes', 'CollecteController');

Route::get('/updateSolr', 'CollecteController@updateSolr')->name('updateSolr');

Route::apiResource('jobs', 'JobController');

Route::get(
    'collectes/{collecte}/relationships/job',
    [
        'uses' => 'CollecteRelationshipController'. '@job',
        'as' => 'collectes.relationships.job',
    ]
);

Route::get(
    'collectes/{collecte}/job',
    [
        'uses' => 'CollecteRelationshipController' . '@job',
        'as' => 'collectes.job',
    ]
);
