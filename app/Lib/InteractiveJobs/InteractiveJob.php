<?php

namespace App\Lib\InteractiveJobs;

use App\Lib\InteractiveJobs\Models\Job;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;
use App\Lib\InteractiveJobs\Contracts\Reportable;


class InteractiveJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $jobModel;
    protected $report;

    public function __construct(Job $jobModel)
    {
        $this->jobModel = $jobModel;
        $this->report = collect([]);
    }

    public function handle()
    {
        $this->registerContext();
        $this->beforeStart();
        try {
            app()->call([$this, 'execute']);
            $this->onFinish();
        } catch (\Throwable $e) {
            $this->onFail($e);
        } finally {
            $this->detachContext();
        }

    }

    protected function context()
    {
        return $this->jobModel;
    }

    protected function registerContext()
    {
        Config::set('jobs.context', $this->context());
    }

    protected function detachContext()
    {
        Config::set('jobs.context', null);
    }

    protected function beforeStart()
    {
        $this->jobModel->activate();
    }

    protected function onFinish()
    {
        $this->jobModel->finish(true);
    }

    protected function onFail(\Throwable $e)
    {
        if ($this instanceof Reportable) {
            $this->jobModel->report = collect(['error' => $e->getMessage()]);
        }
        if ($this->isWillBeRetry()) {
            $this->jobModel->retryAfterFail();
        } else {
            $this->jobModel->finish(false);
        }
        $this->fail($e);
    }

    protected function isWillBeRetry(): bool
    {
        return is_null($this->job->maxTries())
            || $this->attempts() < $this->job->maxTries();
    }

    protected function isRetried(): bool
    {
        return $this->jobModel->attempts > 1;
    }

}
