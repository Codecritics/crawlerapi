<?php


namespace App\Lib\InteractiveJobs\Logger;

use Monolog\Logger;

class JobLogger
{
    public function __invoke()
    {
        $monolog = new Logger('job_logger');
        $level = config('logging.channels.job_logger.level');
        $handler = new JobLogDbHandler($level);
        $monolog->pushHandler($handler);
        return $monolog;
    }
}
