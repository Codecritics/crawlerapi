<?php


namespace App\Lib\InteractiveJobs\Logger;


use App\Lib\InteractiveJobs\Entity\JobContext;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\AbstractProcessingHandler;

class JobLogDbHandler extends AbstractProcessingHandler
{

    protected function write(array $record): void
    {
        $jobContext = Config::get('jobs.context');
        if ($jobContext instanceof Model) {
            DB::table('job_logs')->insert([
                'loggable_id' => $jobContext['id'],
                'loggable_type' => $jobContext['type'],
                'message' => $record['message'],
                'level' => $record['level_name'],
                'context' => json_encode($record['context'] ?? [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
                'extra' => json_encode($record['extra'] ?? [], JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES),
                'created_at' => $record['datetime']->format('Y-m-d H:i:s'),
            ]);
        }
    }
}
