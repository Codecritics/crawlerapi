<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Origin extends Model
{
    protected $fillable = ['code', 'desc_fr', 'desc_en', 'origin_url', 'langue', 'profondeur_crawl', 'limite_nb_doc', 'a_indexer'];
}
