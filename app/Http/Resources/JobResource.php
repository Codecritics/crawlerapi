<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'collectes',
            'id' => (string)$this->id,
            'attributes' => [
                'queue' => $this->queue,
                'payload' => $this->payload,
                'report' => $this->report,
                'result' => $this->result,
                'progress' => $this->progress,
                'attempts' => $this->attempts,
                'created_by' => $this->created_by,
                'started_at' => $this->started_at,
                'finished_at' => $this->finished_at,
            ],
            'links'         => [
                'self' => route('jobs.show', ['job' => $this->id]),
            ],
        ];
    }
}
