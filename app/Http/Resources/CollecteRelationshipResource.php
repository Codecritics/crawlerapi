<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CollecteRelationshipResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'job' => [
                'links' => [
                    'self' => route('collectes.relationships.job', ['collecte' => $this->id]),
                    'related' => route('collectes.job', ['collecte' => $this->id]),
                ],
                'data' => new JobIdentifierResource($this->job),
            ],
        ];
    }
}
