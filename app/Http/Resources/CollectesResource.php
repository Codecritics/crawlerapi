<?php

namespace App\Http\Resources;

use App\Lib\InteractiveJobs\Models\Job;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class CollectesResource extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => CollecteResource::collection($this->collection),
        ];
    }


    public function with($request)
    {
        $job = $this->collection->flatMap(
            function ($collecte) {
                return $collecte->job;
            }
        );
        return [
            'links' => [
                'self' => route('jobs.index'),
            ],
            'included' => $this->withIncluded($job),
        ];
    }

    private function withIncluded(Collection $included)
    {
        return $included->map(
            function ($include) {
                if ($include instanceof Job) {
                    return new JobResource($include);
                }
            }
        );
    }
}
