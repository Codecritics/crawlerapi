<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CollecteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'type' => 'collectes',
            'id' => (string)$this->id,
            'attributes' => [
                'name' => $this->name,
                'result' => $this->result,
                'started_at' => $this->started_at,
                'finished_at' => $this->finished_at,
            ],
            'relationships' => new CollecteRelationshipResource($this),
            'links' => [
                'self' => route('collectes.show', ['collecte' => $this->id]),
            ],
        ];
    }
}
