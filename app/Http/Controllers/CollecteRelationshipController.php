<?php

namespace App\Http\Controllers;

use App\Collecte;
use App\Http\Resources\JobResource;
use Illuminate\Http\Request;

class CollecteRelationshipController extends Controller
{
    public function job(Collecte $collecte)
    {
        return new JobResource($collecte->job()->getChild());
    }

}
