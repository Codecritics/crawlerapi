<?php

namespace App\Http\Controllers;

use App\Lib\InteractiveJobs\Models\JobLog;
use Illuminate\Http\Request;

class JobLogController extends Controller
{

    public function index()
    {
        $logs = JobLog::groups()->paginate();
        return view('logs.index', ['logs' => $logs]);
    }

    public function show(JobLog $log)
    {
        $logs = JobLog::job($log)->paginate();
        return view('logs.show', ['logs' => $logs, 'group' => $log]);
    }

    public function destroy(JobLog $log)
    {
        JobLog::job($log)->delete();
        return redirect()->route('logs.index');
    }

    public function showGroup($type, $id)
    {
        $log =  JobLog::where(['loggable_type' => $type, 'loggable_id' => $id])->firstOrFail();
        $logs = JobLog::job($log)->paginate();
        return view('logs.show', ['logs' => $logs, 'group' => $log]);
    }
}
