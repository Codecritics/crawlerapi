<?php

namespace App\Http\Controllers;

use App\Collecte;
use App\Http\Requests\StoreCollecte;
use App\Http\Resources\CollecteResource;
use App\Http\Resources\CollectesResource;
use App\Jobs\UpdateSolr;
use App\Lib\InteractiveJobs\Contracts\JobDefinitionRepository;
use App\Lib\InteractiveJobs\Models\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class CollecteController extends Controller
{
    /**
     * @var JobDefinitionRepository
     */
    protected $repository;

    public function __construct(JobDefinitionRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('json');
    }

    /**
     * @SWG\Get(
     *     path="/api/collectes",
     *     description="Get all the collectes with their corresponding jobs",
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function index()
    {
        return new CollectesResource(Collecte::with('job')->paginate());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @SWG\Post(
     *     path="/api/create",
     *     description="Return the created job of the collecte",
     *     @SWG\Parameter(
     *         name="url",
     *         in="header",
     *         type="string",
     *         description="The origin url to crawl",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="depth",
     *         in="header",
     *         type="integer",
     *         description="The depth of the Crawler",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="docLimit",
     *         in="header",
     *         type="integer",
     *         description="The limit number of documents returned by the Crawler",
     *         required=true,
     *     ),
     *     @SWG\Parameter(
     *         name="language",
     *         in="header",
     *         type="string",
     *         description="The language of the origin",
     *         required=true,
     *     ),
     *      @SWG\Parameter(
     *         name="fonctions",
     *         in="header",
     *         type="string",
     *         description="A function code",
     *         required=true,
     *     ),
     *      @SWG\Parameter(
     *         name="secteurs",
     *         in="header",
     *         type="string",
     *         description="A sector code",
     *         required=true,
     *     ),
     *      @SWG\Parameter(
     *         name="typesinfos",
     *         in="header",
     *         type="string",
     *         description="A info type code",
     *         required=true,
     *     ),
     *      @SWG\Parameter(
     *         name="themes",
     *         in="header",
     *         type="string",
     *         description="A theme type code",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Unprocessed Data"
     *     )
     * )
     */
    public function store(StoreCollecte $request)
    {
        if ($request->wantsJson()) {
            try {
                $definition = $this->repository->findByName('LaunchCollecte');
                abort_if(!$definition, 404);
                if (!empty($definition->rules())) {
                    try {
                        $this->validate($request, $definition->rules());
                    } catch (ValidationException $e) {
                        return $e;
                    }
                }
                $jobModel = Job::create([
                    'command' => $definition->command(),
                    'queue' => $definition->queue(),
                    'payload' => $definition->handlePayload($request),
                    'created_by' => Auth::user()->id ?? 1,
                ]);
                return response()->json([
                        'job' => $jobModel]
                );
            } catch (\Exception $e) {
                report($e);
            }

        }
    }

    /**
     * @SWG\Get(
     *     path="/api/collecte/{collecte}",
     *     description="Return a specific collecte with his corresponding job",
     *     @SWG\Parameter(
     *         name="id",
     *         in="path",
     *         type="integer",
     *         description="Id of the collecte",
     *         required=true,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *     ),
     *     @SWG\Response(
     *         response=422,
     *         description="Missing Data"
     *     )
     * )
     */
    public function show(Collecte $collecte)
    {
        CollecteResource::withoutWrapping();

        return new CollecteResource($collecte);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Collecte $collecte
     * @return \Illuminate\Http\Response
     */
    public function edit(Collecte $collecte)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Collecte $collecte
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Collecte $collecte)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Collecte $collecte
     * @return \Illuminate\Http\Response
     */
    public function destroy(Collecte $collecte)
    {
        //
    }

    /**
     * @return array
     */
    public function updateSolr(Request $request)
    {
        UpdateSolr::dispatch($request->query)->onQueue('long-running-queue');
        return ['status' => 'Job queued!'];
    }
}
