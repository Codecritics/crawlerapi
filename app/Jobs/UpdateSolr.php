<?php

namespace App\Jobs;

use App\Services\DummyService;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Symfony\Component\HttpFoundation\InputBag;

class UpdateSolr implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var InputBag
     */
    private $params;

    public function __construct(InputBag $params)
    {
        $this->params = $params;
    }

    public function tags()
    {
        return ['loggable'];
    }

    public function handle()
    {
        $res = DB::select('call solrUpdate("' . $this->params->get('application') . '")');
        Http::get($this->params->get('url'));
    }

}
