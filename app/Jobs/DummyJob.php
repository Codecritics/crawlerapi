<?php

namespace App\Jobs;

use App\Lib\InteractiveJobs\InteractiveJob;
use App\Services\DummyService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class DummyJob extends InteractiveJob
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function execute(DummyService $service)
    {
        $payload = $this->jobModel->payload;
        $service->dummyJobLogic(
            $payload['loop'], $payload['delay']
        );
    }
}
