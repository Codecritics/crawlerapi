<?php

namespace App\Jobs;

use App\Collecte;
use App\Lib\InteractiveJobs\Contracts\Reportable;
use App\Lib\InteractiveJobs\InteractiveJob;
use App\Origin;
use App\Services\CollecteCrawler;
use App\Services\ExcludeGivenUrls;
use GuzzleHttp\RequestOptions;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Crawler\Crawler;
use Spatie\Crawler\CrawlProfiles\CrawlInternalUrls;

class LaunchCollecte extends InteractiveJob implements Reportable
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;


    public function execute()
    {
        $name = $this->jobModel->payload['nom'];

        $this->jobModel->updateProgress(10, 'Url Content was loaded');

        $collecte = Collecte::firstWhere('nom', $name);
        $origin = Origin::firstWhere('desc_fr', $name);
        $originCode = "";

        $explodedOriginCode = explode("-", $originCode);
        if (count($explodedOriginCode) > 2) {
            $originCode = $explodedOriginCode[0] . "-" . $explodedOriginCode[1];
        }
        if (!is_null($collecte)) {
            $urls = $collecte->urls;
            $collecte->result = [];
            foreach ($urls as $url) {
                $originFromCollecte = Origin::firstWhere('origin_url', $url);
                $excludedUrls = DB::table('filter')->where('origin_code', $originFromCollecte->code)->first()->URL_Pas_Crawl ?? "";

                $explodedOriginCode = explode("-", $originFromCollecte->code);
                $originCode = $originFromCollecte->code;
                if (count($explodedOriginCode) > 2) {
                    $originCode = $explodedOriginCode[0] . "-" . $explodedOriginCode[1];
                }
                Log::info('URLs de collecte ' . implode(",", $urls) . " avec une limite de " . $originFromCollecte->limite_nb_doc . " et une profondeur de " . $originFromCollecte->profondeur_crawl);
                $indexParams = [
                    'secteurs' => [$originFromCollecte->secteurs_code],
                    'origins' => [$originCode],
                    'application_name' => [$originFromCollecte->application_name],
                    'a_indexer' => [$originFromCollecte->a_indexer],
                    'authgroups' => [$originFromCollecte->authgroups],
                    'fonctions' => [$originFromCollecte->fonction_code],
                    'themes' => [$originFromCollecte->themes_code],
                    'typesInfos' => [$originFromCollecte->type_infos_code]
                ];

                Crawler::create([
                    RequestOptions::COOKIES => true,
                    RequestOptions::CONNECT_TIMEOUT => 60,
                    RequestOptions::TIMEOUT => 60,
                    RequestOptions::ALLOW_REDIRECTS => true])
                    ->setUserAgent('dream-crawler')
                    ->setMaximumDepth($originFromCollecte->profondeur_crawl)
                    ->setCrawlProfile(new CrawlInternalUrls($originFromCollecte->origin_url))
                    ->setCrawlProfile(new ExcludeGivenUrls($excludedUrls))
                    ->setTotalCrawlLimit($originFromCollecte->limite_nb_doc)
                    ->setCrawlObserver(new CollecteCrawler($indexParams, $originFromCollecte->langue, $collecte))
                    ->startCrawling($url);
            }
            $this->jobModel->collecte()->save($collecte);
        } else if (!is_null($origin)) {
            $originCode = $origin->code;
            $excludedUrls = DB::table('filter')->where('origin_code', $origin->code)->first()->URL_Pas_Crawl ?? "";
            $collecte = Collecte::where('nom', $origin->desc_fr)->first() ?? Collecte::create([
                    'nom' => $origin->desc_fr,
                    "urls" => [$origin->origin_url]
                ]);
            $collecte->result = [];
            $indexParams = [
                'secteurs' => [$origin->secteurs_code],
                'fonctions' => [$origin->fonction_code],
                'authgroups' => [$origin->authgroups],
                'a_indexer' => [$origin->a_indexer],
                'application_name' => [$origin->application_name],
                'origins' => [$originCode],
                'themes' => [$origin->themes_code],
                'typesInfos' => [$origin->type_infos_code],
            ];
            Crawler::create([
                RequestOptions::COOKIES => true,
                RequestOptions::CONNECT_TIMEOUT => 60,
                RequestOptions::TIMEOUT => 60,
                RequestOptions::ALLOW_REDIRECTS => true])
                ->setUserAgent('dream-crawler')
                ->setMaximumDepth($origin->profondeur_crawl)
                ->setCrawlProfile(new CrawlInternalUrls($origin->origin_url))
                ->setCrawlProfile(new ExcludeGivenUrls($excludedUrls))
                ->setTotalCrawlLimit($origin->limite_nb_doc)
                ->setCrawlObserver(new CollecteCrawler($indexParams, $origin->langue, $collecte))
                ->startCrawling($origin->origin_url);
            $this->jobModel->collecte()->save($collecte);
        } else {
            abort(404, "No origin or collectes found!");
        }


    }

}
