<?php

namespace App\Services;

use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlProfiles\CrawlProfile;

class ExcludeGivenUrls extends CrawlProfile
{
    /**
     * @example "url1,url2,url3"
     */
    private $setOfUrls;

    public function __construct(string $setOfUrls)
    {

        $this->setOfUrls = $setOfUrls;
    }

    public function shouldCrawl(UriInterface $url): bool
    {
        return !in_array($url, explode(",", $this->setOfUrls));
    }
}
