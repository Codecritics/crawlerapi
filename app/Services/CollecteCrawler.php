<?php


namespace App\Services;


use App\Events\UpdateCollectesStatus;
use App\Collecte;
use Carbon\Carbon;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Log;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\UriInterface;
use Spatie\Crawler\CrawlObservers\CrawlObserver;

class CollecteCrawler extends CrawlObserver
{

    protected $language;
    protected $collecte;
    protected $urlsArray;
    protected $count;
    protected $indexParameters;
    protected $url;

    protected $counter = 0;


    public function __construct($indexParameters, $language, $collecte)
    {
        $this->indexParameters = $indexParameters;
        $this->collecte = $collecte;
        $this->language = $language;
        $this->urlsArray = array();
        $this->url = '';
        $this->count = 0;
    }

    public function willCrawl(UriInterface $url)
    {
        Log::info("Itération #{$this->count} {$url}  will crawl");
        $this->count++;
    }


    /**
     * @inheritDoc
     */
    public function crawled(UriInterface $url, ResponseInterface $response, ?UriInterface $foundOnUrl = null)
    {
        Log::info("{$url} has been crawled with response code {$response->getStatusCode()}");
        $subject = (string)$url;
        $pattern = '/\w{3,4}($|\?)/';
        $match = preg_match($pattern, $subject, $matches);
        $type_doc = "html";
        $json_data = [];
        if ($match == 1) {
            switch ($matches[0]) {
                case "png" :
                    $type_doc = "png";
                    break;
                case "jpeg" :
                    $type_doc = "jpeg";
                    break;
                case "":
                case "html" :
                    $type_doc = "html";
                    break;
                case "pdf" :
                    $type_doc = "pdf";
                    break;
                case "xml" :
                    $type_doc = "xml";
                    break;
                case "doc" :
                    $type_doc = "doc";
                    break;
            }
        }

        if ($type_doc == "xml" || $type_doc == "pdf" || $type_doc == "html") {
            $json_data = [
                'url' => (string)$url,
                'fonctions' => $this->indexParameters["fonctions"],
                'origines' => $this->indexParameters["origins"],
                'authgroups' => $this->indexParameters["authgroups"],
                'application_name' => $this->indexParameters["application_name"],
                'secteurs' => $this->indexParameters["secteurs"],
                'a_indexer' => $this->indexParameters["a_indexer"],
                'themes' => $this->indexParameters["themes"],
                'types_infos' => $this->indexParameters["typesInfos"],
                'counter' => $this->counter++,
                'Lang' => $this->language,
                'Type' => $type_doc,
            ];
        }

        array_push($this->urlsArray, $json_data);


    }

    /**
     * @inheritDoc
     */
    public function crawlFailed(UriInterface $url, RequestException $requestException, ?UriInterface $foundOnUrl = null)
    {
        if ($response = $requestException->getResponse()) {
            $this->crawled($url, $response, $foundOnUrl);
        }
    }

    public function finishedCrawling()
    {
        $this->collecte->finished_at = Carbon::now();
        $this->collecte->result = array_merge($this->collecte->result ?? [], $this->urlsArray);
        Log::info("The origin {$this->url} has successfully crawled");
    }
}
