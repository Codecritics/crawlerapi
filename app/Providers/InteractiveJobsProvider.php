<?php

namespace App\Providers;

use App\Lib\InteractiveJobs\Contracts\JobDefinitionRepository;
use App\Lib\InteractiveJobs\JobDefinitionConfigRepository;
use App\Lib\InteractiveJobs\JobMenuComposer;
use App\Lib\InteractiveJobs\JobModelObserver;
use App\Lib\InteractiveJobs\Models\Job;
use Illuminate\Queue\Events\JobProcessing;
use Illuminate\Queue\QueueManager;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\JobPayload;

class InteractiveJobsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            JobDefinitionRepository::class,
            JobDefinitionConfigRepository::class
        );
    }

    public function provides()
    {
        return [JobDefinitionRepository::class];
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Job::observe(JobModelObserver::class);
        View::composer(['jobs._menu'], JobMenuComposer::class);
    }
}
