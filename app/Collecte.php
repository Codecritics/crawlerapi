<?php

namespace App;

use App\Lib\InteractiveJobs\Models\Job;
use Illuminate\Database\Eloquent\Model;

class Collecte extends Model
{
    protected $fillable = ['nom', 'urls', 'result', 'finished'];

    protected $casts = [
        'urls' => 'array',
        'result' => 'array'
    ];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
