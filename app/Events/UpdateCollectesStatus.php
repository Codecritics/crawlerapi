<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UpdateCollectesStatus
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $status;
    public $url;
    public $duration;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($url, $status, $duration)
    {
        $this->url = $url;
        $this->status = $status;
        $this->duration = $duration;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        // return new PrivateChannel('collecte');
        return new Channel('collecte');
    }
}
