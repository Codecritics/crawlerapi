<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOriginsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('origins', function (Blueprint $table) {
            $table->id();
            $table->string('code',20)->unique();
            $table->string('desc_fr',255)->nullable();
            $table->string('origin_url',255)->nullable();
            $table->string('langue',255)->nullable();
            $table->integer('profondeur_crawl')->nullable();
            $table->integer('limite_nb_doc')->nullable();
            $table->string('desc_en',255)->nullable();
            $table->boolean('a_indexer')->nullable();
            $table->string('parent',45)->default('NULL');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('origins');
    }
}
