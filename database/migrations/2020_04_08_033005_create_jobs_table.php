<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('queue')->default('default');
            $table->text('payload')->nullable();
            $table->text('report')->nullable();
            $table->string('state')->nullable();
            $table->integer('progress')->nullable();
            $table->string('command')->nullable();
            $table->smallInteger('attempts')->unsigned()->default(0);
            $table->integer('created_by')->unsigned()->nullable();
            $table->dateTime('created_at')->default(Carbon::now());
            $table->dateTime('finished_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
